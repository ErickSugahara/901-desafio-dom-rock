import React, { useState, useEffect } from 'react';

import { Container } from './styles';

import { CSSTransition } from "react-transition-group";

export default function Card(props) {

  const [card, setCard] = useState(props.card);
  const [percent, setPercent] = useState(0);

  useEffect(() => {
    let aux = Math.floor((card.found / card.total)*100);
    if (isNaN(aux)) {
      aux = 0;
    }
    setPercent(aux);
  }, [card]);

  useEffect(() => {
    setCard(props.card);
  }, [props.card]);

  return (
    <Container>
      <h4 id='card-tittle'>{card.name}</h4>
      <div className='inner-cicle'>
        <div className='inner-container'>
          <CSSTransition
            in={true}
            key={card.key}
            timeout={1000}
            unmountOnExit
            classNames='percent'
            appear={true}
          >
            <div className='fluid' style={{ height:`${ percent }%`, background: card.color }} /> 
          </CSSTransition>
        </div>
      </div>
      <span id="footer">{card.found} de {card.total}</span>
    </Container>
  );
}
