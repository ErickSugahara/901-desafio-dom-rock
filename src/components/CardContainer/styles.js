import styled from 'styled-components';

export const Container = styled.div`
	background: #bfc8d6;
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: space-between;
	align-items: stretch;
	align-content: space-between;
	min-height: 100px;
	margin-left: 10px;
	margin-right: 10px;
	border-radius: 10px; /*Utilizar número fixo para não ter problemas com proporção */
	padding: 3vw;

`;
