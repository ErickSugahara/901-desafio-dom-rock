import faker from 'faker';

const INITIAL_STATE = [];

export default function cards(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "DATA_GENERATE_MOCK":
      let database = [];
      for (let i=0; i < 4000; i++) {

        let type = faker.random.number(3);
        let situation = faker.random.number(3);
        let filial = faker.random.number(1);

        let item = { 
          id: faker.random.uuid,
          date: faker.date.between(new Date(), new Date(new Date().setFullYear(new Date().getFullYear() - 2))),
          type: type,
          situation: situation,
          filial: filial
        }
        
        database.push(item);
      }
      return { data: database }
    default:
      return state
  }
}