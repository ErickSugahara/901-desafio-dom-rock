import { lastDayOfMonth, startOfMonth, isAfter, isBefore, endOfDay } from 'date-fns';

const INITIAL_STATE = {
  data: [
    {
      index: 1,
      name: "Obrigações geradas",
      total: 0,
      found: 0,
      color: '#ffbb00'
    },
    {
      index: 2,
      name: "Obrigações validadas no PVA",
      total: 0,
      found: 0,
      color: '#00ff99'
    },
    {
      index: 3,
      name: "Relatórios e verificações",
      total: 0,
      found: 0,
      color: '#0080ff'
    },
    {
      index: 4,
      name: "Obrigações transmitidas",
      total: 0,
      found: 0,
      color: '#0048ff'
    },
  ]
};

export default function cards(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "GENERATE_MOCK":
      return {
        data: [
          {
            index: 1,
            name: "Obrigações geradas",
            total: 116,
            found: 25,
            color: '#ffbb00'
          },
          {
            index: 2,
            name: "Obrigações validadas no PVA",
            total: 116,
            found: 50,
            color: '#00ff99'
          },
          {
            index: 3,
            name: "Relatórios e verificações",
            total: 116,
            found: 75,
            color: '#0080ff'
          },
          {
            index: 4,
            name: "Obrigações transmitidas",
            total: 116,
            found: 100,
            color: '#0048ff'
          },
        ]
      };
    case "CARDS_FILTER_DATA":
      if (action.filters.list === undefined) {
        return state;
      }

      let new1 = {
        index: 1,
        name: "Obrigações geradas",
        total: 0,
        found: 0,
        color: '#ffbb00'
      };

      let new2 = {
        index: 2,
        name: "Obrigações geradas",
        total: 0,
        found: 0,
        color: '#00ff99'
      };

      let new3 = {
        index: 3,
        name: "Obrigações geradas",
        total: 0,
        found: 0,
        color: '#0080ff'
      }

      let new4 = {
        index: 4,
        name: "Obrigações geradas",
        total: 0,
        found: 0,
        color: '#0048ff'
      }

      let totalFound = 0;

      action.filters.list.map(data => {
        //Verifica se está dentro do type
        let selectedDate = new Date(action.filters.date);
        if (action.filters.check) {
          if (data.type === action.filters.type) {
            //Verifica se a ocorrencia está dentro do mês
            if (isAfter(data.date, startOfMonth(selectedDate)) && isBefore(data.date, endOfDay(lastDayOfMonth(selectedDate)))) {
              switch (data.situation) {
                case 0:
                  new1.found++;
                  totalFound++;
                  break;
                case 1:
                  new2.found++;
                  totalFound++;
                  break;
                case 2:
                  new3.found++;
                  totalFound++;
                  break;
                case 3:
                  new4.found++;
                  totalFound++;
                  break;
                default:
                  break;
              }
            }
          }
        } else if (!action.filters.check && !data.filial) {
          if (data.type === action.filters.type) {
            //Verifica se a ocorrencia está dentro do mês
            if (isAfter(data.date, startOfMonth(selectedDate)) && isBefore(data.date, endOfDay(lastDayOfMonth(selectedDate)))) {
              switch (data.situation) {
                case 0:
                  new1.found++;
                  totalFound++;
                  break;
                case 1:
                  new2.found++;
                  totalFound++;
                  break;
                case 2:
                  new3.found++;
                  totalFound++;
                  break;
                case 3:
                  new4.found++;
                  totalFound++;
                  break;
                default:
                  break;
              }
            }
          }
        }
        return null;
      });

      new1.total = totalFound;
      new2.total = totalFound;
      new3.total = totalFound;
      new4.total = totalFound;

      return {
        ...state, data: [
          new1, new2, new3, new4
        ]
      }
    case "CARDS_CHANGE_CHECK":
      return {
        ...state, check: action.check.check
      }
    default:
      return state
  }
}