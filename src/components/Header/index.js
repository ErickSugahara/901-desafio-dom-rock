import React, { useState, useEffect } from 'react';

import { Container } from './styles';
import { Checkbox } from 'antd';
import { changeCheck } from "../../store/actions/cards";
import { useDispatch } from 'react-redux';

export default function Header() {

  const [check, setCheck] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(changeCheck(check));
  }, []);

  useEffect(() => {
    setCheck(check);
    dispatch(changeCheck(check));
  }, [check]);

  function toggleCheckBox () {
    setCheck(!check);
  }

  return (
      <Container>
        <h1 id="titulo">Dashboard das Obrigações</h1>
        <Checkbox onChange={toggleCheckBox}>Incluir filiais</Checkbox>
      </Container>
  );
}
