import React, { useState, useEffect } from 'react';

import { Container } from './styles';
import Card from '../Card';

export default function CardGroup(props) {

  const [card, setCard] = useState(props.card);
  
  useEffect(() => {
    setCard(props.card);
  }, [props.card])

  return (
    <Container>
      <Card card={card}/>
      <a href="#">Visualizar</a>{/*Setar card link*/}
    </Container>
  );
}
