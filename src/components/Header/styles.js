import styled from 'styled-components';

export const Container = styled.div`

  margin: 10px;
  display: flex;
  justify-content: space-between;
  align-items: flex-end;

  #titulo {
    color: #417ee0;
    margin-bottom: 0;
  }

  .check-label {
    color: black;
    display: flex;
  }

  .check-box {
    margin-right: 5px
  }

`;
