import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Container } from './styles';
import { Select, Button } from 'antd';
import { generateMock } from "../../store/actions/data";
import { filterData } from "../../store/actions/cards";

const { Option } = Select;

const months = [
  {label:"Janeiro", value:"1"}, 
  {label:"Fevereiro", value:"2"}, 
  {label:"Março", value:"3"}, 
  {label:"Abril", value:"4"}, 
  {label:"Maio", value:"5"}, 
  {label:"Junho", value:"6"}, 
  {label:"Julho", value:"7"}, 
  {label:"Agosto", value:"8"}, 
  {label:"Setembro", value:"9"}, 
  {label:"Outubro", value:"10"},
  {label:"Novembro", value:"11"}, 
  {label:"Dezembro", value:"12"}
]

export default function CardContainerHeader() {

  const [years, setYears] = useState([2019]);
  const [selectedYear, setSelectedYear] = useState(2019);
  const [month, setMonth] = useState(10);
  const [type, setType] = useState(0);
  const check = useSelector(state => state.cards.check);
  const list = useSelector(state => state.data.data);
  const dispatch = useDispatch();


  function loadMock() {
    dispatch(generateMock());
  }

  function filter() {
    /**Meses estão na base 1 */
    dispatch(filterData(list, new Date(selectedYear, (month - 1), 1),  type, check));
  }

  useEffect(() => {
  }, [check]);

  useEffect(() => {
    /**Preencher o campo com os últimos 20 anos */
    const currentYear = new Date().getFullYear();
    const firstYear = currentYear - 20;
    let newYears = [];

    for (let i = 0; i <= 20; i++) {
      
      newYears.push(firstYear + i);
    }
    setYears(newYears);

    /**Carregar massa de dados mockados */
    loadMock();
  }, []);

  useEffect(() => {
    /**Filter */
    filter();
  }, [list]);

  function handleChangetype(value) {
    setType(value);
  }

  function handleChangeMonth(value) {
    setMonth(value);
  }

  function handleChangeYear(value) {
    setSelectedYear(value);
  }

  return (
    <Container>
      <Select id="select-type" defaultValue="SPED FISCAL" onChange={handleChangetype}>
        <Option value={0}>SPED FISCAL</Option>
        <Option value={1}>TESTE 1</Option>
        <Option value={2}>TESTE 2</Option>
        <Option value={3}>TESTE 3</Option>
      </Select>
      <Select id="select-month" defaultValue="10" onChange={handleChangeMonth}>
        {
          months.map(month => (
            <Option key={month.value} value={month.value}>{month.label}</Option>
          ))
        }
      </Select>
      <Select id="select-year" defaultValue="2019" onChange={handleChangeYear}>
        {
          years.map(year => (
            <Option key={year} value={year}>{year}</Option>
          ))
        }
      </Select>
      <Button id="btn-search" type="primary" onClick={filter}>Filtrar</Button>
    </Container>
  );
}
