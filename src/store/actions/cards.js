export function generateMock() {
  return { type: 'GENERATE_MOCK' }
}

export function filterData(list, date, type, check) {
  return { type: 'CARDS_FILTER_DATA', filters:{list, date, type, check} }
}

export function changeCheck(check) {
  return { type: 'CARDS_CHANGE_CHECK', check:{check} }
}