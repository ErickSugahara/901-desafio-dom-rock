import styled from 'styled-components';

export const Container = styled.div`

  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 1vw;
  width: 100%;
  height: 4rem;

  #select-type {
    width: 50%;
    margin-right: 5px;
  }

  #select-month {
    width: 30%;
    margin-right: 5px;
  }

  #select-year {
    width: 10%;
    margin-right: 5px;
    min-width: 5rem;
  }

  #btn-search {
    text-transform: none;
    margin: 0;
  }

  .ant-select-selection {
    background-color: #bfc8d6;
    border-color: #5f7394;
  }
`;
