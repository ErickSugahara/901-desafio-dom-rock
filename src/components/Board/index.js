import React, { useState } from 'react';
import { Container } from './styles';
import { Layout, Menu, Icon } from 'antd';
import CardContainer from "../CardContainer";
import SubHeader from "../Header";

const { Header, Sider, Content } = Layout;

export default function Board() {

  const [state, setState] = useState({collapsed: true});

  return (
    <Container>
      <Layout id="components-layout-demo-custom-trigger" 
            style={{ height: "100vh" }}>
        <Sider trigger={null} collapsible collapsed={state.collapsed}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" 
            defaultSelectedKeys={['1']}>
            <Menu.Item key="1">
              <Icon type="user" />
              <span>nav 1</span>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header style={{ background: '#fff', padding: 0 }}>
            <Icon
              className="trigger"
              type={setState ? 'menu-unfold' : 'menu-fold'}
              onClick={() => (
                setState({
                  collapsed: !state.collapsed,
                })
              )}
            />
          </Header>
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              background: '#fff',
              minHeight: 280,
            }}
          >
            {/* Content aqui */}
            <SubHeader />
            <CardContainer />
          </Content>
        </Layout>
      </Layout>
    </Container>
  );
}
