import styled from 'styled-components';

export const Container = styled.div`
  display: 'flex';
  flex-direction: column;
  justify-content: center;
  text-align: center;
  margin-bottom: 10px
`;
