import React from 'react';
import Board from "./components/Board";
import 'antd/dist/antd.css';

function App() {
  return (
    <>
      <Board />
    </>
  );
}

export default App;
