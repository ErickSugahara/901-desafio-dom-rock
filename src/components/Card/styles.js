import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  padding-bottom: 30px;
	margin: 1vw;
  margin-bottom: 10px;
  width: 140px;
  height: 200px;
	flex-flow: column nowrap;
  flex-direction: column;
  justify-content: space-between;
	align-items: center;
  border-radius: 8%;
  transition: transform .2s;
  color: green;
  background: white;
  border-style: solid;
  border-width: 2px;
  border-color: #a3abb8;
  text-align: center;
  
  #card-tittle {
    min-height: 40px;
    color:#32c2b4;
    margin: 10px;
  }

  #footer {
    font-weight: 600;
    min-height: 40px;
    color:#808080;
    margin: 8px;
  }

  .inner-cicle {

    position: relative;
    background: #bfc8d6;
    padding-bottom: 70%;
    width: 70%;
    border-radius: 50%;
    border-style: solid;
    border-width: 2px;
    border-color: #a3abb8;
  }

  .inner-container {

    display: flex;
    flex-direction: column-reverse;
    position: absolute;
    background: white;
    border-radius: 4px;
    border-style: solid;
    border-width: 1px;
    border-color: #a3abb8;
    height: 65%;
    width: 65%;
    margin-left: 17.5%;
    margin-top: 17.5%;
    padding: 2px;
    display: flex;

  }
  
  .fluid {

    z-index: 0;
    position: relative;
    width: 100%;
    background: orange;
    overflow: hidden;
    transition: 1000ms linear;

  }

  .fluid:hover{

    opacity: 0.8;
    cursor: pointer;

  }

  /**show on screen */
  .percent-appear {
    transform: translateY(50%) scaleY(0);
  }

  .percent-appear.percent-appear-active {
    transform: translateY(0%) scaleY(1);
    transition: 1000ms linear;
  }

`;
