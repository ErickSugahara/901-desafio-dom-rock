import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { Container } from './styles';

import CardContainerHeader from '../CardContainerHeader'
import CardGroup from '../CardGroup';

export default function CardContainer() {

  const cards = useSelector(state => state.cards.data);

  useEffect(() => {
  }, [cards])

  return (
    <Container>
      <CardContainerHeader />
      {
        cards.map(card => (
          <CardGroup key={card.index} card={ card } />
        ))
      }
    </Container>
  );
}
