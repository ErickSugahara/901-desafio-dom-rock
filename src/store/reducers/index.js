import { combineReducers } from "redux";

import cards from "./cards";
import data from "./data";

export default combineReducers({
  cards, data
});